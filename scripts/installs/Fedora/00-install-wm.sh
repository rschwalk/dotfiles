#!/bin/bash
#set -e
##################################################################################################################
# Author 	    : 	Richard Schwalk
# Original idea     : 	Erik Dubois
# Website           :   https://twitter.com/richardschwalk
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################

# update the system
sudo dnf -y upgrade


# sudo dnf -y install qtile
# qtile is no more in fedora repo
# so we have a little more work to do
sudo dnf -y install python3-xcffib
pip install psutil
pip install --no-cache-dir cairocffi
pip install qtile

sudo dnf -y install awesome
sudo dnf -y install arc-theme

echo "Installing category System"
# I starting with the xfce spin, so I not need to install the xfce4 specific
# apps
sudo dnf -y install numlockx

echo "Installing category Accessories"
sudo dnf -y install picom
sudo dnf -y install nitrogen
sudo dnf -y install dmenu
sudo dnf -y install arandr
sudo dnf -y install rofi
sudo dnf -y install i3lock
sudo dnf -y install redshift-gtk
sudo dnf -y install slick-greeter
sudo dnf -y install acpi
sudo dnf -y install mpd mpc ncmpcpp
# TOD: Install Arc Icon Themes for the widgets

echo "Installing openbox theme"
git clone https://github.com/dglava/arc-openbox ~/tools/arc-openbox
mkdir ~/.themes
cp -r ~/tools/arc-openbox/* ~/.themes/

echo "Installing Sardi icon theme"
wget -P ~/tools https://sourceforge.net/projects/sardi/files/sardi-icons-9.6-9.tar.gz
mkdir ~/.icons
tar -xzf ~/tools/sardi-icons-9.6-9.tar.gz -C ~/.icons/

sudo dnf -y install alacritty

sudo dnf -y install paper-icon-theme

