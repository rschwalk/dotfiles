fish_config theme choose "nightfox_fish"
# Base16 Shell
# if status --is-interactive
#      set BASE16_SHELL "$HOME/.config/base16-shell/"
#      source "$BASE16_SHELL/profile_helper.fish"
# end

set -x PATH /usr/sbin /home/rschwalk/bin /home/rschwalk/.local/bin /home/rschwalk/.mix /home/rschwalk/.cargo/bin $PATH
set -x PATH /home/rschwalk/tools/JetBrains/pycharm-community-2022.3.1/bin $PATH


set -x LC_ALL en_US.UTF-8

set -x EDITOR nvim

# Fish git prompt
set __fish_git_prompt_showuntrackedfiles 'yes'
set __fish_git_prompt_showdirtystate 'yes'
set __fish_git_prompt_showstashstate ''
set __fish_git_prompt_showupstream 'none'
set -g fish_prompt_pwd_dir_length 3

# colored man output
# from http://linuxtidbits.wordpress.com/2009/03/23/less-colors-for-man-pages/
setenv LESS_TERMCAP_mb \e'[01;31m'       # begin blinking
setenv LESS_TERMCAP_md \e'[01;38;5;74m'  # begin bold
setenv LESS_TERMCAP_me \e'[0m'           # end mode
setenv LESS_TERMCAP_se \e'[0m'           # end standout-mode
setenv LESS_TERMCAP_so \e'[38;5;246m'    # begin standout-mode - info box
setenv LESS_TERMCAP_ue \e'[0m'           # end underline
setenv LESS_TERMCAP_us \e'[04;38;5;146m' # begin underline

# Fish should not add things to clipboard when killing
# See https://github.com/fish-shell/fish-shell/issues/772
set FISH_CLIPBOARD_CMD "cat"

function fish_user_key_bindings
	bind \cz 'fg>/dev/null ^/dev/null'
	if functions -q fzf_key_bindings
		fzf_key_bindings
	end
end

function fish_prompt
	set_color $fish_color_comment
	echo -n "["(date "+%H:%M")"] "
	set_color $fish_color_quote
	echo -n (hostname)
	if [ $PWD != $HOME ]
		set_color $fish_color_normal # foreground
		echo -n ':'
		set_color $fish_color_operator
		echo -n (basename $PWD)
	end
	set_color $fish_color_end
	printf '%s ' (__fish_git_prompt)
	set_color $fish_color_keyword
	echo -n '| '
	set_color $fish_color_normal
end

if set -q SSH_TTY
  set -g fish_color_host brred
end

function mutt
   fish --login -c 'cd ~/Desktop; /usr/bin/neomutt' $argv;
end

abbr --add nv nvim

#source $HOME/.cargo/env
#source ~/.asdf/asdf.fish

# Start X at login
if status is-login
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        exec startx -- -keeptty
    end
end
