[Appearance]
ColorScheme=nightfox_konsole
Font=Fira Code,10,-1,5,50,0,0,0,0,0
UseFontLineChararacters=true

[General]
Command=/usr/bin/zsh
Name=RS
Parent=FALLBACK/

[Scrolling]
HistorySize=10000
ScrollBarPosition=2
