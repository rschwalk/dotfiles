# Configuration for Alacritty, the GPU enhanced terminal emulator.

# Setup term for programs to be able to use it
env:
  TERM: alacritty
  # TERM: xterm-256color

window:
  # Spread additional padding evenly around the terminal content.
  dynamic_padding: true

    # Startup Mode (changes require restart)
    #startup_mode: Fullscreen

    # Window decorations
    #
    # Values for `decorations`:
    #     - full: Borders and title bar
    #     - none: Neither borders nor title bar
    #
    # Values for `decorations` (macOS only):
    #     - transparent: Title bar, transparent background and title bar buttons
    #     - buttonless: Title bar, transparent background and no title bar buttons
  decorations: full

    # Background opacity
    #
    # Window opacity as a floating point number from `0.0` to `1.0`.
    # The value `0.0` is completely transparent and `1.0` is opaque.
    #opacity: 1.0

    # Window title
    #title: Alacritty

    # Allow terminal applications to change Alacritty's window title.
    #dynamic_title: true

    # Decorations theme variant
    #
    # Override the variant of the System theme/GTK theme/Wayland client side
    # decorations. Commonly supported values are `Dark`, `Light`, and `None` for
    # auto pick-up. Set this to `None` to use the default theme variant.
  decorations_theme_variant: Dark

# Sample Font configuration for font: Monoid.
# Feel free to use different font configuration family & face for each sections
font:
  # Normal font face - Also used to draw glyphs on tmux & VIM
  # NOTE: You may use any font you'd like but make sure the normal fonts
  # support ligatures/glyphs. That's used by tmux & VIM to provide a better
  # UI for powerline & tmux bottom bar.
  normal:
    # Font name
    family: "Fira Code NF"
      #family: "Source Code Pro"
    # Font face
    style: Regular

  # Bold font face
  bold:
    family: "Fira Code NF"
    style: Bold

  # Italic font face
  italic:
    family: "Fira Code NF"
    style: Italic

  # Bold italic font face
  # Note: Since i don't have a font italic version of this font, I just specified
  # italic & it just works. You may specifiy the bold italic version if it exists
  # for your font
  bold_italic:
    family: "Fira Code NF"
    style: Bold Italic

  # Font size
  size: 10.0

  # Offset is the extra space around each character. `offset.y` can be thought of
  # as modifying the line spacing, and `offset.x` as modifying the letter spacing
  # I've given in 14 spacing which fits really well with my fonts, you may change it
  # to your convenience but make sure to adjust 'glyph_offset' appropriately post that
  offset:
   x: 0
   y: 1

  # Note: This requires RESTART
  # By default when you change the offset above you'll see an issue, where the texts are bottom
  # aligned with the cursor, this is to make sure they center align.
  # This offset should usually be 1/2 of the above offset-y being set.
  glyph_offset:
   x: 0
  # Keeping this as half of offset to vertically align the text in cursor
   y: 2

  # Better font rendering for mac
  #use_thin_strokes: true

scrolling:
  # Maximum number of lines in the scrollback buffer.
  # Specifying '0' will disable scrolling.
  history: 50000

selection:
  semantic_escape_chars: ",│`|:\"' ()[]{}<>\t"

  # When set to `true`, selected text will be copied to the primary clipboard.
  save_to_clipboard: true

# Live config reload (changes require restart)
live_config_reload: true

# Setup some amazing custom key bindings here - Best thing is you can setup key bindings
# using Mac's 'command' to control your tmux.
# A great reference: https://arslan.io/2018/02/05/gpu-accelerated-terminal-alacritty/#make-alacritty-feel-like-iterm2
#key_bindings:
  # Use command + [ - to go to previous tmux window
  #  - { key: LBracket, mods: Command, chars: "\x5c\x70" }
  #  # Use command + ] - to go to previous tmux window
  #  - { key: RBracket, mods: Command, chars: "\x5c\x6e" }
  #  # ctrl-^ doesn't work in some terminals like alacritty
  #  - { key: Key6, mods: Control, chars: "\x1e" }

alt_send_esc: false

import:
  - ~/.config/alacritty/visual/colors.yml

