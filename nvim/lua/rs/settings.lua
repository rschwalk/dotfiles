local o = vim.opt

o.shell = "zsh"

-- This must be first, because it changes other options as a side effect.
o.compatible = false

o.guicursor = ""

o.nu = true
o.relativenumber = true

-- use spaces instead TABs
o.tabstop=4
o.softtabstop=2
o.shiftwidth=2
-- o.shiftround = true
o.expandtab = true

o.smartindent = true

o.wrap = false

o.swapfile = false
o.backup = false
o.undodir = os.getenv("HOME") .. "/.vimdid"
o.undofile = true

o.hlsearch = false
o.incsearch = true

o.termguicolors = true

o.scrolloff = 8
o.signcolumn = "yes"
o.isfname:append("@-@")

o.updatetime = 50

o.colorcolumn = "120"

-- set the search scan to wrap lines
-- o.wrapscan = true
-- Make command line two lines high
o.ch = 2

-- Make sure that unsaved buffers that are to be put in the background are
-- allowed to go in there (ie. the "must save first" error doesn't come up)
o.hidden = true

-- Make the 'cw' and like commands put a $ at the end instead of just deleting
-- the text and replacing it
o.cpoptions = "ces$"

-- Make the command-line completion better
o.wildmenu = true

-- Add ignorance of whitespace to diff
o.diffopt:append(",iwhite")

-- Use more sense splitting
o.splitbelow = true
o.splitright = true

-- we use lightline no need for additional mode info
o.showmode = false
o.title = false
o.cursorline = true

o.background = "dark"
vim.cmd([[colorscheme gruvbox]])





-- o.list = true
-- o.listchars = "tab:»-,trail:·,extends:»,precedes:«"
-- o.printoptions = "header:0,duplex:long,paper:A4"
-- allow backspacing over everything in insert mode
-- o.backspace = "indent,eol,start"

-- keep 50 lines of command line history
-- o.history = 50
-- show the cursor position all the time
-- o.ruler = true
-- display incomplete commands
-- o.showcmd = true
-- Don't update the display while executing macros
-- o.lazyredraw = true
-- When the page starts to scroll, keep the cursor 8 lines from the top and 8
-- lines from the bottom
-- Permanent undo
-- Set the textwidth to be 80 chars
-- o.textwidth = 80
-- get rid of the silly characters in separators
-- o.fillchars = ""
-- Add the unnamed register to the clipboard
-- o.clipboard:append("unnamedplus")
-- Automatically read a file that has changed on disk
-- o.autoread = true
-- o.grepprg = "grep -nH $*"
-- don't automatically wrap text when typing
-- o.fo:remove("t")
-- Syntax coloring lines that are too long just slows down the world
-- o.synmaxcol = 2048

-- [[ Highlight on yank ]]
-- See `:help vim.highlight.on_yank()`
-- local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
-- vim.api.nvim_create_autocmd('TextYankPost', {
--   callback = function()
--     vim.highlight.on_yank()
--   end,
--   group = highlight_group,
--   pattern = '*',
-- })
--
vim.cmd([[

" set text wrapping toggles
" nmap <silent> <leader>cw :set invwrap<CR>:set wrap?<CR>

" VimWiki configuration
" let g:vimwiki_list = [{'path': '~/MEGA/Linux/vimwiki/',
"                       \ 'syntax': 'markdown', 'ext': '.md'}]
" let g:vimwiki_markdown_link_ext = 1

" Neovim terminal settings
" :tnoremap <Esc> <C-\><C-n>
" :tnoremap <A-h> <C-\><C-n><C-w>h
" :tnoremap <A-j> <C-\><C-n><C-w>j
" :tnoremap <A-k> <C-\><C-n><C-w>k
" :tnoremap <A-l> <C-\><C-n><C-w>l
" :nnoremap <A-h> <C-w>h
" :nnoremap <A-j> <C-w>j
" :nnoremap <A-k> <C-w>k
" :nnoremap <A-l> <C-w>l
" :au BufEnter * if &buftype == 'terminal' | :startinsert | endif

" Press Space to turn off highlighting and clear any message already displayed.
" :nnoremap <silent> <Space>n :nohlsearch<Bar>:echo<CR>
" nnoremap <left> :bp<CR>
" nnoremap <right> :bn<CR>
" map <Leader>n <esc>:b#<CR>

" easier moving of code blocks
" Try to go into visual mode (v), thenselect several lines of code here and
" then press ``>`` several times.
" vnoremap < <gv  " better indentation
" vnoremap > >gv  " better indentation

" Session management
" nmap <F3> :mksession! ./.session<cr>
" nmap <F4> :source ./.session<cr>

" set enc=utf-8


" Run the last shell command
" nnoremap <leader>rl :!!<CR>

" limit the text width in mutt
" au BufRead /tmp/mutt-* set tw=72
"
" autocmd FileType python set colorcolumn=80
" autocmd FileType elixir set colorcolumn=120
" autocmd FileType fsharp set colorcolumn=120
" autocmd FileType cpp set colorcolumn=100
"autocmd FileType cpp highlight ColorColumn ctermbg=darkgray

" cpp options
" autocmd FileType cpp set makeprg=make\ -j16
" autocmd FileType cpp nnoremap <leader>rm :make<CR>
" autocmd FileType cpp nnoremap <leader>rc :make clean<CR>
" autocmd FileType cpp nnoremap <leader>cm :! cmake ..<CR>

" " Follow Rust code style rules
" au Filetype rust source ~/.config/nvim/scripts/spacetab.vim
" au Filetype rust set colorcolumn=100

" disable arrow keys
" inoremap <up> <nop>
" inoremap <down> <nop>
" inoremap <right> <nop>
" inoremap <left> <nop>
" noremap <up> <nop>
" noremap <down> <nop>
" noremap <right> <nop>
" noremap <left> <nop>


" Useful settings
" set history=700
" set undolevels=700

" Make search case insensitive
" set ignorecase
" set smartcase

" Disable stupid backup and swap files - they trigger too many events
" for file system watchers
" set nobackup
" set nowritebackup
" set noswapfile

" noremap <leader>gpl :-1read ~/dotfiles/nvim/templates/gpl.templ<CR>wi

" set nofoldenable



"------------------
" Plugin settings
"==================



" Neovim settings
"-----------------------------------------------------------------------------
" let g:python2_host_prog = '/usr/bin/python2'
" let g:python3_host_prog = '/usr/bin/python3'


" delimitMate settings
"-----------------------------------------------------------------------------
" let delimitMate_expand_cr = 1
" au FileType fsharp let b:delimitMate_quotes = "\" "

" fzf settings
"-----------------------------------------------------------------------------

" autocmd BufReadPost *.rs setlocal filetype=rust

" language server protocol
"-----------------------------------------------------------------------------
""let g:LanguageClient_serverCommands = {
""      \ 'rust': ['rustup', 'run', 'stable', 'rls'],
""      \ 'c': ['ccls', '--log-file=/tmp/cc.log'],
""      \ 'cpp': ['ccls', '--log-file=/tmp/cc.log'],
""      \ 'cuda': ['ccls', '--log-file=/tmp/cc.log'],
""      \ 'objc': ['ccls', '--log-file=/tmp/cc.log'],
""      \ 'python': ['/usr/local/bin/pyls'],
""      \ 'fsharp': ['dotnet', '/home/rschwalk/.config/nvim/plugged/Ionide-vim/fsac/fsautocomplete.dll'],
""      \ }
""let g:LanguageClient_autoStart=1
""let g:LanguageClient_loadSettings = 1
""let g:LanguageClient_settingsPath = "/home/rschwalk/.config/nvim/settings.json"
""" Valid Options: "All" | "No" | "CodeLens" | "Diagnostics"
""let g:LanguageClient_useVirtualText = "No"
""""let g:LanguageClient_diagnosticsList = 'Location'
""set signcolumn=yes
""nnoremap <F5> :call LanguageClient_contextMenu()<CR>
""nnoremap <silent> K :call LanguageClient_textDocument_hover()<CR>
""nnoremap <silent> gd :call LanguageClient_textDocument_definition()<CR>
""nnoremap <silent> <F2> :call LanguageClient_textDocument_rename()<CR>
""nnoremap <silent> gr :call LanguageClient#textDocument_references({'includeDeclaration': v:false})<cr>
""
""augroup LanguageClient_config
""  au!
""  au BufEnter * let b:Plugin_LanguageClient_started = 0
""  au User LanguageClientStarted setl signcolumn=yes
""  au User LanguageClientStarted let b:Plugin_LanguageClient_started = 1
""  au User LanguageClientStopped setl signcolumn=auto
""  au User LanguageClientStopped let b:Plugin_LanguageClient_started = 0
""  au CursorMoved * if b:Plugin_LanguageClient_started | sil call LanguageClient#textDocument_documentHighlight() | endif
""augroup END
""
""" bases
""nn <silent> xb :call LanguageClient#findLocations({'method':'$ccls/inheritance'})<cr>
""" bases of up to 3 levels
""nn <silent> xB :call LanguageClient#findLocations({'method':'$ccls/inheritance','levels':3})<cr>
""" derived
""nn <silent> xd :call LanguageClient#findLocations({'method':'$ccls/inheritance','derived':v:true})<cr>
""" derived of up to 3 levels
""nn <silent> xD :call LanguageClient#findLocations({'method':'$ccls/inheritance','derived':v:true,'levels':3})<cr>
""
""" caller
""nn <silent> xc :call LanguageClient#findLocations({'method':'$ccls/call'})<cr>
""" callee
""nn <silent> xC :call LanguageClient#findLocations({'method':'$ccls/call','callee':v:true})<cr>
""
""" $ccls/member
""" nested classes / types in a namespace
""nn <silent> xs :call LanguageClient#findLocations({'method':'$ccls/member','kind':2})<cr>
""" member functions / functions in a namespace
""nn <silent> xf :call LanguageClient#findLocations({'method':'$ccls/member','kind':3})<cr>
""" member variables / variables in a namespace
""nn <silent> xm :call LanguageClient#findLocations({'method':'$ccls/member'})<cr>
""
""nn xx x
""
""nn <silent> xh :call LanguageClient#findLocations({'method':'$ccls/navigate','direction':'L'})<cr>
""nn <silent> xj :call LanguageClient#findLocations({'method':'$ccls/navigate','direction':'D'})<cr>
""nn <silent> xk :call LanguageClient#findLocations({'method':'$ccls/navigate','direction':'U'})<cr>
""nn <silent> xl :call LanguageClient#findLocations({'method':'$ccls/navigate','direction':'R'})<cr>
""
""" racer + rust
""" https://github.com/rust-lang/rust.vim/issues/192
"""let g:rustfmt_command = "rustfmt +nightly"
""let g:rustfmt_autosave = 1
""let g:rustfmt_emit_files = 1
""let g:rustfmt_fail_silently = 0
""let g:rust_clip_command = 'xclip -selection clipboard'
"""let g:racer_cmd = "/usr/bin/racer"
"""let g:racer_experimental_completer = 1
""let $RUST_SRC_PATH = systemlist("rustc --print sysroot")[0] . "/lib/rustlib/src/rust/src"
""
"""au BufWrite * :Autoformat
""
""" Completion with deoplete
""let g:deoplete#enable_at_startup = 1
""
""let g:SuperTabDefaultCompletionType = "<c-n>"

" echodoc settings
" let g:echodoc#enable_at_startup = 1
" let g:echodoc#type = 'signature'

"let g:rooter_manual_only = 1

" Elixir settings
"**** "let g:alchemist_tag_disable = 1
" let g:alchemist_tag_map = 'gd'
" autocmd BufWritePost *.exs,*.ex silent :!mix format %

" Completion with ncm2
" set shortmess+=c
"autocmd BufEnter * call ncm2#enable_for_buffer()
" set completeopt=noinsert,menuone,noselect
"let g:UltiSnipsExpandTrigger="<c-l>"
" let g:UltiSnipsJumpForwardTrigger="<c-b>"
" let g:UltiSnipsJumpBackwardTrigger="<c-z>"

"-----------------------------------------------------------------------------
" FSwitch mappings
"-----------------------------------------------------------------------------
"nmap <silent> <leader>of :FSHere<CR>
"nmap <silent> <leader>ol :FSRight<CR>
"nmap <silent> <leader>oL :FSSplitRight<CR>
"nmap <silent> <leader>oh :FSLeft<CR>
"nmap <silent> <leader>oH :FSSplitLeft<CR>
"nmap <silent> <leader>ok :FSAbove<CR>
"nmap <silent> <leader>oK :FSSplitAbove<CR>
"nmap <silent> <leader>oj :FSBelow<CR>
"nmap <silent> <leader>oJ :FSSplitBelow<CR>

" NERDTree
" map <leader>i :NERDTreeToggle<CR>
" let NERDTreeChDirMode = 2     "setting root dir in NT also sets VIM's cd
" let NERDTreeWinSize = 35
"------------------
" Color settings
"==================


" Show whitespace
" MUST be inserted BEFORE the colorscheme command
" autocmd ColorScheme * highlight ExtraWhitespace ctermbg=darkred guibg=darkred
" au InsertLeave * match ExtraWhitespace /\s\+$/
" autocmd BufWritePre * :%s/\s\+$//e

"call togglebg#map("<F9>")

""      colorscheme Tomorrow-Night
""      let g:airline_theme='tomorrow'

"hi! Normal ctermbg=NONE guibg=NONE
"hi! NonText ctermbg=NONE guibg=NONE


" if filereadable(expand("~/.vimrc_background"))
"   let base16colorspace=256
"   "set termguicolors
"   source ~/.vimrc_background
" else
"   set background=dark
"   colorscheme gruvbox
" endif

"set termguicolors
"set background=light
"colorscheme solarized

" highlight Comment cterm=italic gui=italic


"**** autocmd FileType python highlight ColorColumn guibg=DimGrey

]])
