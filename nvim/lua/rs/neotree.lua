vim.cmd([[ let g:neo_tree_remove_legacy_commands = 1 ]])

require("neo-tree").setup()

local key_map = vim.api.nvim_set_keymap
key_map(
  "n",
  "<leader>o",
  ":Neotree toggle<CR>",
  { noremap = true, silent = true }
)
