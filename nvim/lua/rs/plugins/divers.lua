return {
  {

    -- Polyglot loads language support on demand!
    --'sheerun/vim-polyglot',
    'tpope/vim-rhubarb',
    -- compare directories
    'will133/vim-dirdiff',
    -- finishing caracter pars eg. "", (), [] some smart ways
    'Raimondi/delimitMate',
    -- change sorrounding characters
    'tpope/vim-surround',
    -- see searches during typing
    'wincent/loupe',
    -- C/C++ switch between .h and .cpp
    'derekwyatt/vim-fswitch',
    -- C/C++ pull definitions from header files
    'derekwyatt/vim-protodef',
    -- Detect tabstop and shiftwidth automatically
    'tpope/vim-sleuth',
  },
}
