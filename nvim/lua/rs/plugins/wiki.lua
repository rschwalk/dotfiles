return {
  'serenevoid/kiwi.nvim',
  dependencies = {
    "nvim-lua/plenary.nvim"
  },
  opts = {
    {
      name = "wiki",
      path = "/home/rschwalk/SynologyDrive/Documents/wiki"
    },
    {
      name = "blog",
      path = "/home/username/wiki_2"
    }
  },
  keys = {
    { "<leader>ww", ":lua require(\"kiwi\").open_wiki_index()<cr>", desc = "Open Wiki index" },
    { "<leader>wb", ":lua require(\"kiwi\").open_wiki_index(\"blog\")<cr>", desc = "Open index of personal wiki" },
    { "<leader>t", ":lua require(\"kiwi\").todo.toggle()<cr>", desc = "Toggle Markdown Task" }
  },
  lazy = true
}
