return {
    -- "gc" to comment visual regions/lines
    'numToStr/Comment.nvim',
    lazy = false,
    config = function()
        require('Comment').setup()
    end,
}
