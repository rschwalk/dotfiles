return {
    {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v3.x',
        lazy = true,
        config = false,
        init = function()
            -- Disable automatic setup, we are doing it manually
            vim.g.lsp_zero_extend_cmp = 0
            vim.g.lsp_zero_extend_lspconfig = 0
        end,
    },
    {
        'williamboman/mason.nvim',
        lazy = false,
        config = true,
    },


    -- Autocompletion
    {
        'hrsh7th/nvim-cmp',
        event = 'InsertEnter',
        dependencies = {
           { "hrsh7th/cmp-buffer" },
           { "hrsh7th/cmp-path" },
           { "hrsh7th/cmp-nvim-lua" },
           { "hrsh7th/cmp-vsnip" },
           { "Saecki/crates.nvim" },
           { "f3fora/cmp-spell" },
           { "hrsh7th/cmp-cmdline" },
           { "saadparwaiz1/cmp_luasnip" },
           -- Snippets
           {
               'L3MON4D3/LuaSnip',
               dependencies = "rafamadriz/friendly-snippets",
               opts = { history = true, updateevents = "TextChanged,TextChangedI" },
               config = function(_, opts)
                   require("luasnip").config.set_config(opts)

                   -- vscode format
                   require("luasnip.loaders.from_vscode").lazy_load()
                   require("luasnip.loaders.from_vscode").lazy_load { paths = vim.g.vscode_snippets_path or "" }

                   -- snipmate format
                   require("luasnip.loaders.from_snipmate").load()
                   require("luasnip.loaders.from_snipmate").lazy_load { paths = vim.g.snipmate_snippets_path or "" }

                   -- lua format
                   require("luasnip.loaders.from_lua").load()
                   require("luasnip.loaders.from_lua").lazy_load { paths = vim.g.lua_snippets_path or "" }

                   vim.api.nvim_create_autocmd("InsertLeave", {
                       callback = function()
                           if
                               require("luasnip").session.current_nodes[vim.api.nvim_get_current_buf()]
                               and not require("luasnip").session.jump_active
                               then
                                   require("luasnip").unlink_current()
                               end
                           end,
                       })
               end,
           },
        },
        config = function()
            -- Here is where you configure the autocompletion settings.
            local lsp_zero = require('lsp-zero')
            lsp_zero.extend_cmp()

            -- And you can configure cmp even more, if you want to.
            local cmp = require('cmp')
            -- local cmp_action = lsp_zero.cmp_action()
            local cmp_select = {behavior = cmp.SelectBehavior.Select}
            local luasnip = require('luasnip')
            cmp.setup({
                sources = {
                    {name = 'path'},
                    {name = 'nvim_lsp'},
                    {name = 'nvim_lua'},
                    { name = 'luasnip' },
                    { name = 'buffer'},
                },
                formatting = lsp_zero.cmp_format(),
                mapping = cmp.mapping.preset.insert({
                    -- ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
                    -- ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
                    ['<C-y>'] = cmp.mapping.confirm({ select = true }),
                    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
                    ['<C-d>'] = cmp.mapping.scroll_docs(4),
                    ['<C-Space>'] = cmp.mapping.complete(),
                    ["<C-n>"] = cmp.mapping(function(fallback)
                        if cmp.visible() then
                            cmp.select_next_item(cmp_select)
                            -- You could replace the expand_or_jumpable() calls with expand_or_locally_jumpable() 
                            -- that way you will only jump inside the snippet region
                        elseif luasnip.expand_or_jumpable() then
                            luasnip.expand_or_jump()
                        else
                            fallback()
                        end
                    end, { "i", "s" }),

                    ["<C-p>"] = cmp.mapping(function(fallback)
                        if cmp.visible() then
                            cmp.select_prev_item(cmp_select)
                        elseif luasnip.jumpable(-1) then
                            luasnip.jump(-1)
                        else
                            fallback()
                        end
                    end, { "i", "s" }),
                }),
            })
            -- cmp.setup({
            --     formatting = lsp_zero.cmp_format(),
            --     mapping = cmp.mapping.preset.insert({
            --         ['<C-Space>'] = cmp.mapping.complete(),
            --         ['<C-u>'] = cmp.mapping.scroll_docs(-4),
            --         ['<C-d>'] = cmp.mapping.scroll_docs(4),
            --         ['<C-f>'] = cmp_action.luasnip_jump_forward(),
            --         ['<C-b>'] = cmp_action.luasnip_jump_backward(),
            --     })
            -- })
        end
    },

    -- LSP
    {
        'neovim/nvim-lspconfig',
        cmd = {'LspInfo', 'LspInstall', 'LspStart'},
        event = {'BufReadPre', 'BufNewFile'},
        dependencies = {
            {'hrsh7th/cmp-nvim-lsp'},
            {'williamboman/mason-lspconfig.nvim'},
        },
        config = function()
            -- This is where all the LSP shenanigans will live
            local lsp_zero = require('lsp-zero')
            lsp_zero.extend_lspconfig()

            lsp_zero.on_attach(function(client, bufnr)
                -- see :help lsp-zero-keybindings
                -- to learn the available actions
                local opts = {buffer = bufnr, remap = false}

                vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
                vim.keymap.set("n", "gr", function() require('telescope.builtin').lsp_references() end, opts)
                vim.keymap.set("n", "<leader>ds", function() require('telescope.builtin').lsp_document_symbols() end, opts)
                vim.keymap.set("n", "<leader>ws", function() require('telescope.builtin').lsp_dynamic_workspace_symbols() end, opts)
                vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, {buffer = bufnr, remap = false, desc = 'Show documentation'} )
                vim.keymap.set("n", "<leader>vws", function() vim.lsp.buf.workspace_symbol() end, opts)
                vim.keymap.set("n", "<leader>vd", function() vim.diagnostic.open_float() end, opts)
                vim.keymap.set("n", "<leader>dn", function() vim.diagnostic.goto_next() end, opts)
                vim.keymap.set("n", "<leader>dp", function() vim.diagnostic.goto_prev() end, opts)
                vim.keymap.set("n", "<leader>ca", function() vim.lsp.buf.code_action() end, opts)
                vim.keymap.set("n", "<leader>vrr", function() vim.lsp.buf.references() end, opts)
                vim.keymap.set("n", "<leader>rn", function() vim.lsp.buf.rename() end, opts)
                vim.keymap.set("i", "<C-h>", function() vim.lsp.buf.signature_help() end, opts)
                vim.keymap.set("n", "<leader>os", ":ClangdSwitchSourceHeader<CR>", {desc = "Switch header and source"})
            end)

            require('mason-lspconfig').setup({
                ensure_installed = {
                "rust_analyzer",
                "lua_ls",
                },
                handlers = {
                    lsp_zero.default_setup,
                    lua_ls = function()
                        -- (Optional) Configure lua language server for neovim
                        local lua_opts = lsp_zero.nvim_lua_ls()
                        require('lspconfig').lua_ls.setup(lua_opts)
                    end,
                }
            })
            vim.diagnostic.config({
                virtual_text = true
            })
        end
    }
}

  -- ------------------------------------------------------------------



        -- lsp_zero.set_preferences({
            --     suggest_lsp_zero_servers = false,
            --     sign_icons = {
                --         error = 'E',
                --         warn = 'W',
                --         hint = 'H',
                --         info = 'I'
                --     }
                -- })
                --
                --
                --
                -- lsp_zero.setup()

