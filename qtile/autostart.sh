#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

run xmodmap ~/.xmodmap
run numlockx on
run nm-applet
# run xfce4-power-manager
run xfsettingsd
run /usr/libexec/polkit-gnome-authentication-agent-1 &
# run /home/rschwalk/dotfiles/dual.sh
run nitrogen --restore
run picom --config /home/rschwalk/.config/awesome/picom.conf &
# run megasync &
run /usr/libexec/geoclue-2.0/demos/agent
# run /usr/bin/gnome-keyring-daemon --start --components=ssh &
run redshift-gtk
run blueman-applet
eval $(ssh-agent -c)
run /usr/bin/flatpak run --branch=stable --arch=x86_64 --command=synology-drive com.synology.SynologyDrive start

