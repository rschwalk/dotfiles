#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

run xmodmap ~/.xmodmap
run numlockx on
run nm-applet
# run xfce4-power-manager
run xfsettingsd
run /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
# run /usr/libexec/xfce-polkit
# run /home/rschwalk/dotfiles/dual.sh
run nitrogen --restore
run picom --config /home/rschwalk/.config/awesome/picom.conf
run megasync &
run /usr/libexec/geoclue-2.0/demos/agent
run /usr/bin/gnome-keyring-deamon --start --components=ssh
run redshift-gtk
run blueman-applet
# eval $(ssh-agent -c)
run /usr/bin/flatpak run --branch=stable --arch=x86_64 --command=synology-drive com.synology.SynologyDrive start




#run dex $HOME/.config/autostart/arcolinux-welcome-app.desktop
##run xrandr --output VGA-1 --primary --mode 1360x768 --pos 0x0 --rotate normal
##run xrandr --output HDMI2 --mode 1920x1080 --pos 1920x0 --rotate normal --output HDMI1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output VIRTUAL1 --off
##autorandr horizontal
#run nm-applet
##run caffeine
#run pamac-tray
#run variety
#run xfce4-power-manager
#run blueberry-tray
#run /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
#run numlockx on
##run volumeicon
#run redshift-gtk
##run nitrogen --restore
#run conky -c $HOME/.config/awesome/system-overview
##you can set wallpapers in themes as well
#feh --bg-fill /usr/share/backgrounds/arcolinux/arco-wallpaper.jpg &
##run applications from startup
##run firefox
##run atom
##run dropbox
##run insync start
##run spotify
##run ckb-next -b
##run discord
##run telegram-desktop
